import Vue from 'vue'
import Vuex, { ActionContext } from 'vuex'
import { moment } from '@/utils/calendar';
import { Actions, Shift, shiftTypes } from './models';

Vue.use(Vuex);

export type State = {
  locale: string;
  shiftDate: moment.Moment;
  selectedShift: Shift;
  calendarDate: moment.Moment;
  shifts: Shift[];
}

const today = moment();

export default new Vuex.Store<State>({
  state: {
    locale: 'uk_UA',
    shiftDate: today,
    selectedShift: shiftTypes[3],
    calendarDate: today,
    shifts: shiftTypes,
  },
  mutations: {
    [Actions.selectShiftDate] (state, date: moment.Moment) {
      state.shiftDate = date;
    },
    [Actions.selectShift] (state, shift: Shift) {
      state.selectedShift = shift;
    },
    [Actions.selectCalendarDate] (state, date: moment.Moment) {
      state.calendarDate = date;
    }
  },
  actions: {
    selectDate (context: ActionContext<State, {}>, date: moment.Moment) {
      context.commit(Actions.selectShiftDate, date);
    },
    selectShift (context: ActionContext<State, {}>, shift: Shift) {
      context.commit(Actions.selectShift, shift);
    },
    selectCalendarDate (context: ActionContext<State, {}>, date: moment.Moment) {
      context.commit(Actions.selectCalendarDate, date);
    }
  },
})
