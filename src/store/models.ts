import { moment } from '@/utils/calendar';

export type Shift = {
  type: string,
  display: string,
}

export const shiftTypes: Shift[] = [{
  type: 'day',
  display: 'У день'
}, {
  type: 'night',
  display: 'У ніч'
}, {
  type: 'from_night',
  display: 'З ночі'
}, {
  type: 'day_off',
  display: 'Вихідний'
}];

export const DATE_FORMAT = 'D M YYYY';

export const shiftColors = {
  day: '#FFE74C',
  night: '#FF5964',
  from_night: '#35A7FF',
  day_off: '#6BF178'
};

/* day: '#AD7A99',
  night: '#B2CEDE',
  from_night: '#8CDFD6',
  day_off: '#6DC0D5'
*/

export enum Actions {
  selectShiftDate = 'selectShiftDate',
  selectShift = 'selectShift',
  selectCalendarDate = 'selectCalendarDate',
}
