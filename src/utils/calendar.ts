import moment, { Moment } from 'moment';
import { DATE_FORMAT } from '@/store/models';

export { moment };

export type CalendarData = {
  lastMonthDayWeekIndex: number,
  lastMonthDayIndex: number,
  firstMonthDayIndex: number,
  previousMonthStartDate: number,
  previousMonthEndDate: number,
  previousMonthIndex: number,
  previousMonthYear: number,
  nextMonthIndex: number,
  nextMonthYear: number,
}

export type Day = {
  date: moment.Moment,
  isCurrentMonth?: boolean,
  isToday?: boolean,
}

export type Calendar = Day[][]

type PreviousMonthInfo = {
  index: number,
  start: number,
  end: number,
  year: number,
}

const getPreviousMonthDiff = (firstDayOfMonthIndex: number, date: Date | Moment): PreviousMonthInfo => {
  if (firstDayOfMonthIndex !== 0) {
    const previousMonth = moment(date).subtract(1, 'month');
    const previousMonthAmountOfDays = previousMonth.daysInMonth();

    return {
      index: previousMonth.month(),
      start: previousMonthAmountOfDays - firstDayOfMonthIndex + 1,
      end: previousMonthAmountOfDays,
      year: previousMonth.year()
    };
  }

  return {
    index: 0,
    start: 0,
    end: 0,
    year: 0
  };
};

export const getWeekdays = (): string[] => {
  return moment.weekdaysShort(true);
};

export const generateCalendarData = (momentDate: Moment): CalendarData => {
  const FIRST_MONTH_DAY = 1;
  const FIRST_WEEK_DAY_SHIFT = 1;

  const date = moment(momentDate);
  const amountOfDays = momentDate.daysInMonth();
  const firstMonthDayIndexRaw = momentDate.date(FIRST_MONTH_DAY).day() - FIRST_WEEK_DAY_SHIFT;
  const firstMonthDayIndex = firstMonthDayIndexRaw < 0 ? 6 : firstMonthDayIndexRaw;
  const previousMonth = getPreviousMonthDiff(firstMonthDayIndex, momentDate);
  const lastMonthDayWeekIndexRaw = date.date(amountOfDays).day() - FIRST_WEEK_DAY_SHIFT;
  const lastMonthDayWeekIndex = lastMonthDayWeekIndexRaw < 0 ? 6 : lastMonthDayWeekIndexRaw;
  const nextMonth = moment(momentDate).add(1, 'month');

  return {
    lastMonthDayWeekIndex,
    lastMonthDayIndex: amountOfDays,
    firstMonthDayIndex,
    previousMonthStartDate: previousMonth.start,
    previousMonthEndDate: previousMonth.end,
    previousMonthIndex: previousMonth.index,
    previousMonthYear: previousMonth.year,
    nextMonthIndex: nextMonth.month(),
    nextMonthYear: nextMonth.year()
  }
};

export const createMonthArrayData = (date: Moment): Calendar => {
  const WEEKDAYS_LENGTH = 7;

  const calendarRange: Calendar = [];
  const days = [];
  const now = moment();
  const momentDate = moment(date);
  const calendar = generateCalendarData(momentDate);
  const monthIndex = momentDate.month();
  const thisMonth = now.month();
  const year = momentDate.year();
  const today = thisMonth === monthIndex ? now.date() : null;

  if (calendar.previousMonthEndDate !== 0) {
    for (let i = calendar.previousMonthStartDate; i <= calendar.previousMonthEndDate; i++) {
      days.push({
        date: moment(`${i} ${calendar.previousMonthIndex + 1} ${calendar.previousMonthYear}`, DATE_FORMAT),
        isCurrentMonth: false,
      });
    }
  }

  for (let i = 1; i <= calendar.lastMonthDayIndex; i++) {
    days.push({
      date: moment(`${i} ${monthIndex + 1} ${year}`, DATE_FORMAT),
      isCurrentMonth: true,
      isToday: today === i,
    });
  }

  for (let i = 1; i < WEEKDAYS_LENGTH - calendar.lastMonthDayWeekIndex; i++) {
    days.push({
      date: moment(`${i} ${calendar.nextMonthIndex + 1} ${calendar.nextMonthYear}`, DATE_FORMAT),
      isCurrentMonth: false,
    });
  }

  let counter = 0;

  for (let i = 0; i < days.length; i++) {
    if (i !== 0 && i % 7 === 0) {
      counter++;
    }

    if (typeof calendarRange[counter] === 'undefined') {
      calendarRange[counter] = [];
    }

    calendarRange[counter][i % 7] = days[i];
  }

  return calendarRange;
};

export const getYearRange = (): number[] => {
  const currentYear = moment().year();
  const maximum = currentYear + 30;
  const minimum = currentYear - 30;
  const years = [];

  for (let i = minimum; i <= maximum; i++) {
    years.push(i);
  }

  return years;
};
