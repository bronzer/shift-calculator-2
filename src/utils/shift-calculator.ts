import { Calendar, Day } from '@/utils/calendar';
import { Shift, shiftTypes } from '@/store/models';
import * as moment from 'moment';

export type ShiftCalendar = ({shift: Shift} & Day)[][]

export const getDaysDifference = (dateOne: moment.Moment, dateTwo: moment.Moment): number => {
  const MS_IN_SEC = 1000;
  const SEC_IN_HOUR = 3600;
  const HOURS_IN_DAY = 24;
  const difference = dateOne.valueOf() - dateTwo.valueOf();

  return Math.ceil(difference / MS_IN_SEC / SEC_IN_HOUR / HOURS_IN_DAY);
};

export const calculateShift = (currentShift: Shift, differenceInDays: number): Shift => {
  const currentShiftIndex = shiftTypes.indexOf(currentShift);
  const koeficient = shiftTypes.length;
  const daysLeft = Math.abs(differenceInDays % koeficient);

  if (differenceInDays < 0) {
    const index = currentShiftIndex - daysLeft;

    return shiftTypes[index < 0 ? koeficient + index : index];
  }

  const index = currentShiftIndex + daysLeft;

  return shiftTypes[index >= koeficient ? index - koeficient : index];
};

export const fillCalendarDates = (calendar: Calendar, firstMonthShift: Shift): ShiftCalendar => {
  const shiftCalendar: ShiftCalendar = [];

  let shiftIndex = shiftTypes.indexOf(firstMonthShift);

  for (let week = 0; week < calendar.length; week++) {
    const weekSet = calendar[week];

    shiftCalendar[week] = [];

    for (let day = 0; day < weekSet.length; day++) {
      shiftCalendar[week][day] = {
        ...weekSet[day],
        shift: shiftTypes[shiftIndex]
      };

      shiftIndex++;

      if (shiftIndex === shiftTypes.length) {
        shiftIndex = 0;
      }
    }
  }

  return shiftCalendar;
};
