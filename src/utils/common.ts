export function hexToRgbA (hex: string, alpha: number = 1) {
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    const arr = hex.substring(1).split('');
    const arrFormatted = arr.length === 3 ? [arr[0], arr[0], arr[1], arr[1], arr[2], arr[2]] : arr;
    const sixteenColor = parseInt('0x' + arrFormatted.join(''));
    return `rgba(${[(sixteenColor >> 16) & 255, (sixteenColor >> 8) & 255, sixteenColor & 255].join(',')},${alpha})`;
  }
  throw new Error('Bad Hex');
}
